#!/bin/sh

# Run this script when cloning the repository to install git hooks from the .hooks folder

GREEN="\e[32m"
ENDCOLOR="\e[0m"

cp -r .hooks/* .git/hooks

echo "${GREEN}Successfully enabled git hooks.${ENDCOLOR}"