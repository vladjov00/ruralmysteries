# ------------------------- Begin Generic CMake Variable Logging ------------------

# /*    C++ comment style not allowed    */

if(NOT WIN32)
    string(ASCII 27 Esc)
    set(EndColor "${Esc}[m")
    set(ColorBold  "${Esc}[1m")
    set(Red         "${Esc}[31m")
    set(Green       "${Esc}[32m")
    set(Yellow      "${Esc}[33m")
    set(Blue        "${Esc}[34m")
    set(Magenta     "${Esc}[35m")
    set(Cyan        "${Esc}[36m")
    set(White       "${Esc}[37m")
    set(BoldRed     "${Esc}[1;31m")
    set(BoldGreen   "${Esc}[1;32m")
    set(BoldYellow  "${Esc}[1;33m")
    set(BoldBlue    "${Esc}[1;34m")
    set(BoldMagenta "${Esc}[1;35m")
    set(BoldCyan    "${Esc}[1;36m")
    set(BoldWhite   "${Esc}[1;37m")
endif()

function(print_variables)
    message( NOTICE "-------------------------------------------------------------------------------------")
    message( NOTICE "${BoldMagenta} CMAKE VARIABLES: ${EndColor}")

    # if you are building in-source, this is the same as CMAKE_SOURCE_DIR, otherwise
    # this is the top level directory of your build tree
    message( STATUS "${Cyan}CMAKE_BINARY_DIR: ${EndColor}" ${Green} ${CMAKE_BINARY_DIR} ${EndColor})

    # if you are building in-source, this is the same as CMAKE_CURRENT_SOURCE_DIR, otherwise this
    # is the directory where the compiled or generated files from the current CMakeLists.txt will go to
    message( STATUS "${Cyan}CMAKE_CURRENT_BINARY_DIR: ${EndColor}" ${Green} ${CMAKE_CURRENT_BINARY_DIR} ${EndColor} )

    # this is the directory, from which cmake was started, i.e. the top level source directory
    message( STATUS "${Cyan}CMAKE_SOURCE_DIR:         ${EndColor}" ${Green} ${CMAKE_SOURCE_DIR} ${EndColor} )

    # this is the directory where the currently processed CMakeLists.txt is located in
    message( STATUS "${Cyan}CMAKE_CURRENT_SOURCE_DIR: ${EndColor}" ${Green} ${CMAKE_CURRENT_SOURCE_DIR} ${EndColor} )

    # contains the full path to the top level directory of your build tree
    message( STATUS "${Cyan}PROJECT_BINARY_DIR: ${EndColor}" ${Green} ${PROJECT_BINARY_DIR} ${EndColor} )

    # contains the full path to the root of your project source directory,
    # i.e. to the nearest directory where CMakeLists.txt contains the project() command
    message( STATUS "${Cyan}PROJECT_SOURCE_DIR: ${EndColor}" ${Green} ${PROJECT_SOURCE_DIR} ${EndColor} )

    # set this variable to specify a common place where CMake should put all executable files
    # (instead of CMAKE_CURRENT_BINARY_DIR)
    message( STATUS "${Cyan}EXECUTABLE_OUTPUT_PATH: ${EndColor}" ${Green} ${EXECUTABLE_OUTPUT_PATH} ${EndColor} )

    # set this variable to specify a common place where CMake should put all libraries
    # (instead of CMAKE_CURRENT_BINARY_DIR)
    message( STATUS "${Cyan}LIBRARY_OUTPUT_PATH:     ${EndColor}" ${Green} ${LIBRARY_OUTPUT_PATH} ${EndColor} )

    # tell CMake to search first in directories listed in CMAKE_MODULE_PATH
    # when you use find_package() or include()
    message( STATUS "${Cyan}CMAKE_MODULE_PATH: ${EndColor}" ${Green} ${CMAKE_MODULE_PATH} ${EndColor} )

    # this is the complete path of the cmake which runs currently (e.g. /usr/local/bin/cmake)
    message( STATUS "${Cyan}CMAKE_COMMAND: ${EndColor}" ${Green} ${CMAKE_COMMAND} ${EndColor} )

    # this is the CMake installation directory
    message( STATUS "${Cyan}CMAKE_ROOT: ${EndColor}" ${Green} ${CMAKE_ROOT} ${EndColor} )

    # this is the filename including the complete path of the file where this variable is used.
    message( STATUS "${Cyan}CMAKE_CURRENT_LIST_FILE: ${EndColor}" ${Green} ${CMAKE_CURRENT_LIST_FILE} ${EndColor} )

    # this is linenumber where the variable is used
    message( STATUS "${Cyan}CMAKE_CURRENT_LIST_LINE: ${EndColor}" ${Green} ${CMAKE_CURRENT_LIST_LINE} ${EndColor})

    # this is used when searching for include files e.g. using the find_path() command.
    message( STATUS "${Cyan}CMAKE_INCLUDE_PATH: ${EndColor}" ${Green} ${CMAKE_INCLUDE_PATH} ${EndColor} )

    # this is used when searching for libraries e.g. using the find_library() command.
    message( STATUS "${Cyan}CMAKE_LIBRARY_PATH: ${EndColor}" ${Green} ${CMAKE_LIBRARY_PATH} ${EndColor} )

    # the complete system name, e.g. "Linux-2.4.22", "FreeBSD-5.4-RELEASE" or "Windows 5.1"
    message( STATUS "${Cyan}CMAKE_SYSTEM:${EndColor} " ${Green} ${CMAKE_SYSTEM} ${EndColor} )

    # the short system name, e.g. "Linux", "FreeBSD" or "Windows"
    message( STATUS "${Cyan}CMAKE_SYSTEM_NAME: ${EndColor}" ${Green} ${CMAKE_SYSTEM_NAME} ${EndColor} )

    # only the version part of CMAKE_SYSTEM
    message( STATUS "${Cyan}CMAKE_SYSTEM_VERSION: ${EndColor}" ${Green} ${CMAKE_SYSTEM_VERSION} ${EndColor} )

    # the processor name (e.g. "Intel(R) Pentium(R) M processor 2.00GHz")
    message( STATUS "${Cyan}CMAKE_SYSTEM_PROCESSOR: ${EndColor}" ${Green} ${CMAKE_SYSTEM_PROCESSOR} ${EndColor} )

    # is TRUE on all UNIX-like OS's, including Apple OS X and CygWin
    message( STATUS "${Cyan}UNIX: ${EndColor}" ${Green} ${UNIX} ${EndColor} )

    # is TRUE on Windows, including CygWin
    message( STATUS "${Cyan}WIN32: ${EndColor}" ${Green} ${WIN32} ${EndColor} )

    # is TRUE on Apple OS X
    message( STATUS "${Cyan}APPLE: ${EndColor}" ${Green} ${APPLE} ${EndColor} )

    # set this to true if you don't want to rebuild the object files if the rules have changed,
    # but not the actual source files or headers (e.g. if you changed the some compiler switches)
    message( STATUS "${Cyan}CMAKE_SKIP_RULE_DEPENDENCY: ${EndColor}" ${Green} ${CMAKE_SKIP_RULE_DEPENDENCY} ${EndColor} )

    # since CMake 2.1 the install rule depends on all, i.e. everything will be built before installing.
    # If you don't like this, set this one to true.
    message( STATUS "${Cyan}CMAKE_SKIP_INSTALL_ALL_DEPENDENCY: ${EndColor}" ${Green} ${CMAKE_SKIP_INSTALL_ALL_DEPENDENCY} ${EndColor} )

    # If set, runtime paths are not added when using shared libraries. Default it is set to OFF
    message( STATUS "${Cyan}CMAKE_SKIP_RPATH: ${EndColor}" ${Green} ${CMAKE_SKIP_RPATH} ${EndColor} )

    # set this to true if you are using makefiles and want to see the full compile and link
    # commands instead of only the shortened ones
    message( STATUS "${Cyan}CMAKE_VERBOSE_MAKEFILE: ${EndColor}" ${Green} ${CMAKE_VERBOSE_MAKEFILE} ${EndColor} )

    # the compiler flags for compiling C sources
    message( STATUS "${Cyan}CMAKE_C_FLAGS: ${EndColor}" ${Green} ${CMAKE_C_FLAGS} ${EndColor} )

    # the compiler flags for compiling C++ sources
    message( STATUS "${Cyan}CMAKE_CXX_FLAGS: ${EndColor}" ${Green} ${CMAKE_CXX_FLAGS} ${EndColor} )

    # Choose the type of build.  Example: set(CMAKE_BUILD_TYPE Debug)
    message( STATUS "${Cyan}CMAKE_BUILD_TYPE: ${EndColor}" ${Green} ${CMAKE_BUILD_TYPE} ${EndColor} )

    # if this is set to ON, then all libraries are built as shared libraries by default.
    message( STATUS "${Cyan}BUILD_SHARED_LIBS: ${EndColor}" ${Green} ${BUILD_SHARED_LIBS} ${EndColor} )

    # the compiler used for C files
    message( STATUS "${Cyan}CMAKE_C_COMPILER: ${EndColor}" ${Green} ${CMAKE_C_COMPILER} ${EndColor} )

    # the compiler used for C++ files
    message( STATUS "${Cyan}CMAKE_CXX_COMPILER: ${EndColor}" ${Green} ${CMAKE_CXX_COMPILER} ${EndColor} )

    # if the compiler is a variant of gcc, this should be set to 1
    message( STATUS "${Cyan}CMAKE_COMPILER_IS_GNUCC: ${EndColor}" ${Green} ${CMAKE_COMPILER_IS_GNUCC} ${EndColor} )

    # if the compiler is a variant of g++, this should be set to 1
    message( STATUS "${Cyan}CMAKE_COMPILER_IS_GNUCXX : ${EndColor}" ${Green} ${CMAKE_COMPILER_IS_GNUCXX} ${EndColor} )

    # the tools for creating libraries
    message( STATUS "${Cyan}CMAKE_AR: ${EndColor}" ${Green} ${CMAKE_AR} ${EndColor} )
    message( STATUS "${Cyan}CMAKE_RANLIB: ${EndColor}" ${Green} ${CMAKE_RANLIB} ${EndColor} )

    message( NOTICE "${Magenta}To list ALL CMake variables, run cmake with ${Red}'-DALL_VARS=true' ${Magenta} OR ${Red}'-DALL_VARS=1'${EndColor}")

    message( NOTICE "-------------------------------------------------------------------------------------")

endfunction()
#
#message( STATUS ": " ${} )

# ------------------------- End of Generic CMake Variable Logging ------------------
